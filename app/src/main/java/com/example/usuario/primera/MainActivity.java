package com.example.usuario.primera;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvFechaHora;
    private Button btnObtenerFecha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvFechaHora = (TextView)findViewById(R.id.tvFechayhora);
        btnObtenerFecha = (Button)findViewById(R.id.btnObtener);
        btnObtenerFecha.setOnClickListener(this);
        actualizar();
    }

    private void actualizar() {
        tvFechaHora.setText(new Date().toString());
    }

    @Override
    public void onClick(View view) {
        if (view == btnObtenerFecha){
            actualizar();
        }
    }
}
